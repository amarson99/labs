public class TestExceptions {
    

    public static void main(String[] args) {

        //Create new array of type Account size 3
        Account[] arr = new Account[3];

        //Array of balances to be placed into the Account array
        double[] bals = {15.0, 256.0, 50000.0};

        //Array of names to be places into the Account array
        String[] ns = {"Suzy", "Fingers", "Carrie"};

        //Block that instantiates Account types into the Account array
        //Adding in try/catch blocks as well
        try{
            
            for (int i=0; i<arr.length; i++){

                arr[i] = new SavingsAccount (bals[i], ns[i]); //Cant instantiate type Account!!
            }
        }
        //If above does not work due to the defined Exception (name is "Fingers") then print this
        catch (DodgyNameException e){
            System.out.println("Exception: " + e);
            return; //Ends the main method when there is a "Fingers"
        }

        finally {

            double taxCollected = 0;
            for (Account a: arr){
                taxCollected += (a.getBalance() * 0.4);
            }
            System.out.println(taxCollected);
        }

        /* When this main method is ran with the name "Fingers" the Exception makes the main method
        quit so the taxCollected is never displayed. When there is no "Fingers" the taxCollection is 
        displayed since the main method is not quit */

    }
}