public class TestAccount2 {
    public static void main(final String[] args) {
        
        // Create an array of account objects size 5
        final Account[] arr = new Account[5];

        // Create arrays of balances and names to be put into the array
        final double [] balances = {3000.0, 450.0, 7.0, 777.0, 60.0};
        final String[] names = {"Alex", "Carly", "Bobby", "Marcus", "Tina"};

        // For loop to place balances and names into the Account class array
        //This for loop uses the constructor created in Accounts that takes in a double balances, then a String name
        for (int i=0; i< arr.length; i++){
            arr[i] = new Account(balances[i], names[i]);
        }

        Account.setInterestRate(1.02);

        // For loop that first displays the balance, then adds interest, then displays new balance
        for (Account a: arr){

            System.out.println("Hello, " + a.getName() + "\n" + "Your current balance is: " + a.getBalance());

            a.addInterest();

            System.out.println("After interest, your new balance is: " + a.getBalance());

            System.out.println("\n");

        }

        //TESTING the withdraw methods (with for loops so can test all inputs)

        //Withdraw method with input
        for (Account a: arr){

            System.out.println("Balance: " + a.getBalance());
            System.out.println("Withdraw 300 possible? " + a.withdraw(300)); //Hard-coded withdraw amount for testing purposes
            
        }

        System.out.println("\n");

        //Withdraw method with no input
        for (Account a: arr){

            System.out.println("Balance: " + a.getBalance());

            System.out.println("Withdraw 20 possible? " + a.withdraw());

            System.out.println("LOL withdrew 20 anyways. New balance: " + a.getBalance());
        }


        




    }
}