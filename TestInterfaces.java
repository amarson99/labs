public class TestInterfaces {
    

    public static void main(String[] args) throws DodgyNameException {
        
        //Creating an array of type Detailable with objects type Account and HomeInsurance
        Detailable[] myArray = {
            new CurrentAccount(350.0, "Russel"),
            new SavingsAccount(12.0, "Jill"),
            new HomeInsurance(2.0, 47.0, 8.0)
        };

        //Looping through the array and calling getDetails method
        for (Detailable a: myArray){
            System.out.println(a.getDetails()); 
        }



    }
}