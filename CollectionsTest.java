import java.util.*;
public class CollectionsTest {
    

    public static void main(String[] args) {
        
        //Declaring variable that will reference a HashSet of Account references called accounts (LHS)
        //Also instantiating the HashSet (RHS)

        //Part 1: declare and instantiate a variable that references a HashSet of Account references
        //HashSet<Account> accounts= new HashSet<Account>();

        //Part 2: Re-implement as a TreeSet
        //Pass an instance of AccountComparator to TreeSet constructor
        TreeSet<Account> accounts = new TreeSet<>(new AccountComparator());

        //Adding 3 objects to the HashSet using .add (exception handling here as well)
        try{
            accounts.add(new SavingsAccount(123.0, "Sammy"));
            accounts.add(new SavingsAccount(12.0, "Cat"));
            accounts.add(new SavingsAccount(450.0, "Joe"));
        } 
        catch (DodgyNameException e) {
            //
        }


        //Part 1: Retrieving an Iterator
        //This creates iter of type Iterator that takes in type Account and uses .iterator method 
        Iterator<Account> iter = accounts.iterator();


        //Iterating over the accounts using Iterator
        while (iter.hasNext())
		{
			Account nextAccount = iter.next();
			System.out.println("The name is " +
							nextAccount.getName());
			System.out.println("The balance is " +
							nextAccount.getBalance());
			nextAccount.addInterest();
			System.out.println("The new balance is " +
							nextAccount.getBalance() + "\n");

        }

        //Iterating over the accounts using the Java 5 for each construct
        for (Account a: accounts){
            System.out.println("Name: "  + a.getName());
            System.out.println("Balance: " + a.getBalance());
            a.addInterest();
            System.out.println("New balance: " + a.getBalance());
        }


        //Iterating over the accounts using the Java 8 forEach method;
        accounts.forEach(a -> {
            System.out.println("Name: "  + a.getName());
            System.out.println("Balance: " + a.getBalance());
            a.addInterest();
            System.out.println("New balance: " + a.getBalance());
        } );



    
    }
}