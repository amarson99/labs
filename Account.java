
// Hello! All of these comments are how I am understanding the material. Please let mne know if terminology is wrong.
public abstract class Account implements Detailable {
    
    // VARIABLES/PROPERTIES
    private static double interestRate;
    protected double balance;
    protected String name;

    // METHODS
    
    // setters: These are all METHODS within the Account class that are the way the certain values of a new instance are set.
    //These methods give no output, but take an input that will be what sets the value

    public void setBalance(final double b) {
        balance = b;
    }

    
    //Modify this code so that it throws a DodgyNameException
    //Since name is a parameter, DodgyNameException is thrown
    public void setName(String n) throws DodgyNameException{
        name = n;

        //Checks if name is "Fingers" and throws Exception if it is
        if ("Fingers".equals(name)){
            throw new DodgyNameException();
        }

    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }



    // getters: These are all METHODS within the Account class that are the way certain values of the different instances of Account are returned
    // These methods have an output, type depending on what we are getting

    public double getBalance(){
        return balance;
    }

    public String getName(){
        return name;
    }

    public static double getInterestRate() {
        return interestRate;
    }


    // This is another METHOD that utilizes the static variable interestRate
    //This method has no output nor input
    public abstract void addInterest();

    // Another METHOD that checks if the user has enough money to withdraw amount requested
    //This method will output T/F and takes an input of a double 
    public boolean withdraw(double amount){
        if (balance >= amount){
            return true;
        } else {
            return false;
        }
    }

    //This method returns T/F and utilizes the above withdraw method
    public boolean withdraw(){
        balance = balance -20;
        return withdraw(20);
    }

    //This method is the method implemented from Detailable.
    //Since it is an Account, the only details are name and balance
    public String getDetails(){
        return "Name: " + name + " Balance: " + balance;
    }

    

    // CONSTRUCTORS

    //Modify the constructor sso that they throw a DodgyNameException;
    //Since names are a parameter, the Exception is thrown
    //If one of the names is "Fingers" the Exception will be thrown

    //This is the "custom" constructor that is created that takes in a balance and a String in that order
    public Account(double bal, String n) throws DodgyNameException{
        if ("Fingers".equals(n)){
            throw new DodgyNameException();
        }
        balance = bal;
        name = n;
    }

    // Here is the a constructor which uses "this" to set default parameters (i.e if nothing is inputted when an instance is called)
    public Account(String name) throws DodgyNameException{
        if ("Fingers".equals(name)){
            throw new DodgyNameException();
        }
        this.name=name;
    }

    //Default constructor for case when nothing is inputted
    public Account() {
    }

    


    

    



}