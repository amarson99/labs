public class CurrentAccount
    extends Account {
    

    //CONSTRUCTORS
    public CurrentAccount( double b,  String n) throws DodgyNameException {
        super(b,n);
        if ("Fingers".equals(n)){
            throw new DodgyNameException();
        }
    }

    

    //Overriding the addInterest method
    @Override
    public void addInterest(){
        setBalance(getBalance() * 1.1);
    }
}