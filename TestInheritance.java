public class TestInheritance {
    

    public static void main(String[] args) {
        
        //Declare array of type Account, add 3 elements
        Account[] accounts = {
            new SavingsAccount(2.0, "Suzy"),
            new SavingsAccount(4.0, "Billy"),
            new CurrentAccount(6.0, "Karen")
        };

        //Loop through the array and call addInterest on each one
        for (int i=0; i<accounts.length; i++){

            System.out.println("Name: " + accounts[i].getName() + "\n" + "Balance: " + accounts[i].getBalance());
            accounts[i].addInterest();
            System.out.println("Interest added. Balance : " + accounts[i].getBalance());
            System.out.println("\n");

        }
    }
}