public class SavingsAccount 
    extends Account{

        //Constructor that takes name and balance arguments
        public SavingsAccount(double b, String n) throws DodgyNameException{
            super(b,n);
            if ("Fingers".equals(n)){
                throw new DodgyNameException();
            }
        }

        //Overriding the addInterest method
        @Override
        public void addInterest(){
            setBalance(getBalance()* 1.4);
        }
    }
