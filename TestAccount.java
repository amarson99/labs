// This is a new class, where we put our main (the part that can run) and test it.

public class TestAccount {
    public static void main(String[] args) {
        // Here a new instance of Account is created, called myAccount
        Account myAccount = new Account();
        // The name and balance are then set using the methods defined in the Account class
        myAccount.setName("Alex");
        myAccount.setBalance(123456);

        // The getters are utilized in this print message.
        //System.out.println("The account name is " + myAccount.getName() + '\n' + "The account balance is " + myAccount.getBalance());

        // Creating an array of Account objects
        Account[] arrayOfAccounts = new Account[4];

        double[] amounts = {12345, 4567890, 45, 778};
        String[] names = {"Alex", "Jack", "Nicky", "Warren"};

        // Creating a for-loop that will populate the arrayOfAccounts to have a name and balance
        for (int i=0; i<arrayOfAccounts.length; i++){
            arrayOfAccounts[i] = new Account(amounts[i], names[i]);

        

            System.out.println("Name on account is " + arrayOfAccounts[i].getName() + '\n' + "Current Balance is " + arrayOfAccounts[i].getBalance());
            arrayOfAccounts[i].addInterest();
            System.out.println("New Balance is " + arrayOfAccounts[i].getBalance());
        }
        System.out.println(arrayOfAccounts[0]);
    }
}
        
